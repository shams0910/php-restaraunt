<?php 

include('../DB/baza.php');

if(isset($_GET['id'])){

    $id = $_GET['id'];
    
    $sql = "SELECT * FROM menu WHERE meal_id = $id LIMIT 1";

    $result = mysqli_query($conn, $sql);
    
    $meal = mysqli_fetch_assoc($result);
        
    }
else {
    echo "Item not found";
}



?>

<body>
<?php include('../Common/header.php'); ?>

<div class="container m-5">
    <div class="card fs-3">
        <h5 class="card-header fs-2"><?php echo $meal['title']; ?></h5>
        <div class="card-body">
            <h5 class="card-title">
            
                <span class="badge bg-warning text-dark">At price: $<?php echo $meal['price']; ?></span>

            </h5>
            <p class="card-text"><?php echo $meal['description']; ?></p>
            <div class="d-flex">
                <div>
                <a  href="editMeal.php?id=<?php echo $meal['meal_id']; ?>" 
                    class="btn btn-primary m-2">
                    Edit
                </a>
                </div>
                

                <form action="deleteMeal.php" method="POST" class="m-2">
                    <input type="hidden" name="id_to_delete" value="<?php echo $meal['meal_id']; ?>">
                    <input type="submit" name="delete" value="Delete" class="btn btn-danger">
                </form>    
            </div>    
        </div>
    </div>
</div>

    
</body>