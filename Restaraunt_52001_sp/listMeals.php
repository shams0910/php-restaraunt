
<?php 

include('../DB/baza.php');

// write query for all meals
$sql = 'SELECT meal_id, title, category, url FROM menu;';

// get the result set (set of rows)
$result = mysqli_query($conn, $sql);

// fetch the resulting rows as an array
$meals = mysqli_fetch_all($result, MYSQLI_ASSOC);

// free the $result from memory (good practise)
mysqli_free_result($result);

// close connection
mysqli_close($conn);



?>


<!DOCTYPE html>
<html lang="en">
    
<body>
<?php include('../Common/header.php'); ?>

<div class="container m-5">
    <div class="d-flex flex-row flex-wrap">
        <?php foreach($meals as $meal): ?>

            <div class="card m-2" style="width: 400px;">
                <img src="<?php echo $meal['url']; ?>" class="card-img-top" alt="..." height="300px">
                <div class="card-body">
                    <h5 class="card-title"><?php echo htmlspecialchars($meal['title']); ?></h5>
                    <p class="card-text">
                        Category: <span class="badge bg-secondary"><?php echo htmlspecialchars($meal['category']); ?></span>
                    </p>

                    <a href="detailMeal.php?id=<?php echo $meal['meal_id']; ?>" 
                    class="btn btn-primary">Read more</a>
                    
                    <a href="editMeal.php?id=<?php echo $meal['meal_id']; ?>" 
                    class="my-btn btn-primary">
                        Edit
                    </a>

                    <form action="deleteMeal.php" method="POST" class="mt-2">
                        <input type="hidden" name="id_to_delete" value="<?php echo $meal['meal_id']; ?>">
                        <input type="submit" name="delete" value="Delete" class="btn btn-danger">
                    </form>
                </div>
            </div>
        <?php endforeach; ?> 
    </div>
</div>

    
</body>
</html>

