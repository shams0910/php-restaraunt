<head>
    <link rel="stylesheet" href="../css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<div class="container my-2">
    <ul class="nav">
        <li>
            <h5 class="m-2 logo"> 
                <a href="http://127.0.0.1:8000/" class="logo-url">
                    Restaraunt Management Platform
                </a>
            </h5>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="listMeals.php">Show list of meals</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="addMeal.php">Add a meal to menu</a>
        </li>
    </ul>
</div>