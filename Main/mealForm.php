<div class="m-5">
    <h3>
    <? echo ($is_editing === true) ? "Edit Meal" : "Add Meal" ?> to restaraunt menu
    </h3>

    <form action=<? echo ($is_editing === true) ? "editMeal.php" : "addMeal.php" ?> method="POST" >
        <input type="text" name="meal_id" hidden value="<?= $meal['meal_id'] ?? '' ?>">

        <div class="mb-3">
            <label for="title" class="form-label">Title of the meal</label>
            <br/>
            <input type="text" name="title" required class="form-control"
            value="<?= $meal['title'] ?? '' ?>"
            >
        </div>

        <div class="mb-3">
            <label for="price" class="form-label">Price of the meal</label> 
            <br/>
            <input type="number" name="price" required class="form-control"
            value="<?= $meal['price'] ?? '' ?>"
            >
        </div>

        <div class="mb-3">
            <label for="category" class="form-label">Category of the meal</label>
            <br/>
            <select class="form-select" name="category" aria-label="Default select example">
                <option value="Italian">Italian </option>
                <option value="Mexican">Mexican</option>
                <option value="Indian">Indian </option>
                <option value="Polish">Polish</option>
            </select>
        </div>

        <div class="mb-3">
            <label class="form-label">Description of food/meal</label>
            <textarea name="description" class="form-control" rows="3"><?= $meal['description'] ?? '' ?></textarea>
        </div>

        <div class="mb-3">
            <label for="title" class="form-label">Image url</label>
            <br/>
            <input type="text" name="url" required class="form-control"
            value="<?= $meal['url'] ?? '' ?>"
            >
        </div>
        
        <br/>
        <br/>

        <div>
            <input type="submit" class="btn btn-primary shamsiddin parpiev 52001" 
            value="<? echo ($is_editing === true) ? "Edit Meal" : "Add Meal" ?>">
        </div>
    </form>
    </div>
    shamsiddin parpiev 52001